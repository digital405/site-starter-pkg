## Grunt tasks

List of Grunt commands that are run at the different stages of the workflow.
* default - "grunt:watch"
  * sass hint
  * sass compile
  * javascript hint
  * javascript concatenation
  * copy assets if newer - images/icons/fonts
  * browser sync
* build - "grunt:build"
  * css minify
  * javascript uglify
  * image compression
  * browser sync
  * git
    * create branch
    * semver branch
    * commit message
    * push branch
    * deploy branch
